--SQLite3 schema for lll.db
CREATE TABLE lll (
    id INTEGER PRIMARY KEY,
    logDate DATE NOT NULL,
    comment TEXT,
    lifts JSON NOT NULL, -- [{name, spec}...]
    topSets JSON, -- [{name, {weight, reps, rpe}}...]
    logData JSON NOT NULL
);