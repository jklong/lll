# lll
*Lift Log Lex*

lll takes my gym notebook shorthand and parses it into a database for much easier historical searching, graphing etc

## Testing
To run parsing tests:
1. Be in the root of the project and make sure pytest is installed or venv is activated.
2. `$ pytest`. Seriously, it's that easy.

## Prerequisites
 - `pip install -r requirements.txt`
 - `json1.so` for sqlite3