import unittest
import os
from lll.parser import parse

def test_2021_06_12():
    filename = os.path.abspath('tests/2021-06-12.txt')
    wanted = {
        "date": "2021-06-12",
        "lift": "Pin Squat",
        "spec": "3@9d+4-6%",
        "sets": [(60, None, 7),
            (100, None, 7),
            (140, None, 7),
            (140, None, 7),
            (160, None, 8),
            (170, None, 9),
            (162.5, 4, 9)]
    }
    got = parse(filename)  
    assert got == wanted

def test_2021_06_13():
    filename = os.path.abspath('tests/2021-06-13.txt')
    wanted = {
        "date": "2021-06-13",
        "lift": "Pause Bench",
        "spec": "3@9d+5%",
        "sets": [(60, None, 7),
            (80, None, 7),
            (100, None, 7),
            (105, None, 9),
            (100, 3, 9)],
        "comment": "Left delt pain affecting bench?"
    }
    got = parse(filename)  
    assert got == wanted