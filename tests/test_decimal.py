import unittest
import os
from lll.parser import parse

def test_decimal_rpe():
    filename = os.path.abspath('tests/decimalRpe.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@9",
        "sets": [(60, None, 7.5)]
    }
    got = parse(filename)  
    assert got == wanted

def test_decimal_weight():
    filename = os.path.abspath('tests/decimalWeight.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@9",
        "sets": [(62.5, None, 7)]
    }
    got = parse(filename)  
    assert got == wanted


if __name__ == '__main__':
    unittest.main()