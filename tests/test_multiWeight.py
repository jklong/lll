import unittest
import os
from lll.parser import parse

def test_multiWeightSimple():
    filename = os.path.abspath('tests/multiWeight.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@9",
        "sets": [(60, None, 7),
            (80, None, 7)]
    }
    got = parse(filename)  
    assert got == wanted

def test_multiWeightManySet():
    filename = os.path.abspath('tests/multiWeightManySet.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@9",
        "sets": [(60, None, 7),
            (60, None, 7),
            (80, None, 7),
            (80, None, 8)]
    }
    got = parse(filename)  
    assert got == wanted

if __name__ == '__main__':
    unittest.main()