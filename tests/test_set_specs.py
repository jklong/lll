import unittest
import os
from lll.parser import parse

def test_simple_spec():
    filename = os.path.abspath('tests/simple_spec.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@9",
        "sets": [(60, None, 7)]
    }
    got = parse(filename)  
    assert got == wanted

def test_repeat_spec():
    filename = os.path.abspath('tests/repeat_spec.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@8>9",
        "sets": [(60, None, 7)]
    }
    got = parse(filename)  
    assert got == wanted

def test_simple_drop():
    filename = os.path.abspath('tests/simple_drop_spec.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@9d4-6%",
        "sets": [(60, None, 7)]
    }
    got = parse(filename)  
    assert got == wanted

def test_amrap_drop():
    filename = os.path.abspath('tests/amrap_drop_spec.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@9d+4-6%",
        "sets": [(60, None, 7)]
    }
    got = parse(filename)  
    assert got == wanted


def test_no_rpe_spec():
    filename = os.path.abspath('tests/noRPESpec.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5x5x100",
        "sets": [(60, None, 7), (100, None, 9)]
    }
    got = parse(filename)  
    assert got == wanted