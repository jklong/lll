import unittest
import os
from lll.parser import parse

def test_simple_comment():
    filename = os.path.abspath('tests/comment.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@9",
        "sets": [(60, None, 7)],
        "comment": "This is a test comment?"
    }
    got = parse(filename)  
    assert got == wanted