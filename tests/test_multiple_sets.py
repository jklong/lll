import unittest
import os
from lll.parser import parse

def test_multiSets():
    filename = os.path.abspath('tests/multiSets.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@9",
        "sets": [(60, None, 7),
            (60, None, 8),
            (60, None, 9),
            (60, None, 9)]
    }
    got = parse(filename)  
    assert got == wanted

if __name__ == '__main__':
    unittest.main()