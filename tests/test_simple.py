import unittest
import os
from lll.parser import parse

def test_simple():
    filename = os.path.abspath('tests/simple.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@9",
        "sets": [(60, None, 7)]
    }
    got = parse(filename)  
    assert got == wanted

def test_simple_fail():
    filename = os.path.abspath('tests/simple_fail.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@9",
        "sets": [(60, None, None)]
    }
    got = parse(filename)  
    assert got == wanted

def test_simple_inline_reps():
    filename = os.path.abspath('tests/inline_reps.txt')
    wanted = {
        "date": "2021-06-04",
        "lift": "Squat",
        "spec": "5@9",
        "sets": [(60, 2, 7)]
    }
    got = parse(filename)  
    assert got == wanted


if __name__ == '__main__':
    unittest.main()