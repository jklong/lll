import argparse
import sys
import lll.parser as parser
import lll.db as db
from pprint import pprint

def main():
    parser = argparse.ArgumentParser(description="Parse notebook gym logs into a database with minimal fuss")
    parser.add_argument('--db', default="lll.db", help="The lll.db file to use")
    subparsers = parser.add_subparsers()

    parser_parse = subparsers.add_parser('parse', help="Parse log input")
    parser_parse.add_argument('-f', '--file', nargs='?', type=argparse.FileType('r'), default=sys.stdin, help="A file to parse for workout data")
    parser_parse.set_defaults(func=do_parse)

    parser_dbinit = subparsers.add_parser('init', help='Initialise a database')
    parser_dbinit.set_defaults(func=db_init)

    parser_add = subparsers.add_parser('add', help='Add a new workout to the database')
    parser_add.add_argument('-f', '--file', nargs='?', type=argparse.FileType('r'), default=sys.stdin, help="A file to parse for workout data")
    parser_add.set_defaults(func=do_add)


    args=parser.parse_args()
    if hasattr(args, 'func'):
        args.func(args)
    else:
        parser.print_help()

def do_parse(args):
    log = parser.parse(args.file)
    pprint(log)
    

def db_init(args):
    db.init(args.db)

def do_add(args):
    log = parser.parse(args.file)
    db.add(args.db, log)
