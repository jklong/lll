# from re import I
import pyparsing as pp

def parse(file):
    parser = __init_parser()
    parsed = parser.parseFile(file).asDict()

    #TODO: copy reps from spec to sets if not otherwise specified.
    # As a parse action for an exercise? Will need a parsed spec

    return parsed

def __init_parser():
    """
    Define the parser for the document
    """
    date = pp.Regex("[0-9]{4}-[01][0-9]-[0123][0-9]")
    lift = pp.Regex("[A-Za-z ]+[A-Za-z]")
    set_spec = pp.Regex(r'(?:\d+@(?!\d+\.?\d?@)\d+\.?\d?(?:\>\d+\.?\d?|d\+?\d-?\d?\%)?|\d+x\d+x\d+)') # TODO: Make a set spec its own object

    rpe_number = pp.Regex(r'(?!\d+\.?\d?[@x])1?[\dF]+\.?\d?') # An acceptable RPE number - 1-10 or F - with some extra lookahead to make sure we don't take the next set spec

    COMMA = pp.Suppress(",")
    AT = pp.Suppress("@")
    COMMENT = pp.Suppress("//")

    rpe = rpe_number + COMMA | rpe_number
    rpe_list = pp.Group(pp.OneOrMore(rpe))
    weight =  pp.Word(pp.nums + ".")

    # One or more logged sets at a given weight
    logged_sets = pp.Group(weight + AT + rpe_list | weight + pp.Suppress("x") + pp.Word(pp.nums) + AT + rpe_list)
    logged_sets.setParseAction(parse_sets)
    set_list = pp.Group(pp.OneOrMore(logged_sets))

    comment = pp.Optional(COMMENT + pp.restOfLine).setParseAction(trim_comment)
    
    # The full document, stitch the components together
    parser = date("date") + lift("lift") + set_spec("spec") + set_list("sets") + comment("comment")
    return parser

def parse_sets(tokens):
    tokens = tokens[0]

    reps = None
    if len(tokens) == 2:
        weight, rpe = tokens
    elif len(tokens) == 3:
        weight, reps, rpe = tokens
        reps = int(reps)
    
    tok = [(float(weight), reps, float(r)) if r != 'F' else (int(weight), None, None) for r in rpe]

    return tok

def trim_comment(tokens):
    if len(tokens) == 1:
        tokens = tokens[0].strip(' ')
    return tokens
