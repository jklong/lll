import sqlite3
import os
import json

SCHEMA_FILE = 'schema.sql'

def init(filename):
    if os.path.isfile(filename):
        raise FileExistsError("Database file %s already exists", filename)
    
    if not os.path.isfile(SCHEMA_FILE):
        raise FileNotFoundError("Schema SQL file not found")

    with open(SCHEMA_FILE, 'r') as f:
        sql = str(f.read())

    conn = sqlite3.Connection(filename)

    conn.executescript(sql)
    conn.commit()

    conn.close()
    
def add(dbfile, log):
    if not log:
        raise ValueError("Cannot commit a null log")
    
    if not isinstance(log, dict):
        raise TypeError("log must be a dict")

   # We've received a parsed log. We'll need to synthesise some things to put in the fields and then insert the dict json into the json field 

    conn = sqlite3.Connection(dbfile)
    # TODO: fix for multiple lift logs
    lifts = json.dumps([{'name':log['lift'], 'spec': log['spec']}])
    logData = json.dumps(log['sets'])

    conn.execute('INSERT INTO lll (logDate, comment, lifts, topSets, logData) values (?, ?, ?, ?, ?);', (log['date'], log['comment'] if hasattr(log, 'comment') else None, lifts, None, logData))
    for r in conn.execute('SELECT * FROM lll;'):
        print(r)
    conn.commit()
    conn.close()